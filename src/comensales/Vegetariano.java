package comensales;

import java.util.ArrayList;
import comidas.Comida;

public class Vegetariano extends Comensal {

	private static final Integer valoracionMinima = 85;

	public Vegetariano(Double peso, ArrayList<Comida> comidasFavoritas) {
		super(peso, comidasFavoritas);
	}

	@Override
	public boolean leAgrada(Comida comida) {
		boolean leAgrada = false;
		if (comida.isAptoVegetariano() && (comida.getValoracion() > valoracionMinima)) {
			this.getComidasFavoritas().add(comida);
			leAgrada = true;
		}
		return leAgrada;
	}

	@Override
	public boolean estaSatisfecho() {
		// La condición adicional para estar satisfechos es que ninguna comida de las
		// ingeridas sea abundante.
		Boolean ningunaComidaAbundante = this.getComidasIngeridas().stream().noneMatch(x -> x.esAbundante() == true);
		return condicion1PorcientoDelPesoPropio() && ningunaComidaAbundante;
	}
	
	@Override
	public String toString() {
		return super.toString("Vegetariano");
	}
}
