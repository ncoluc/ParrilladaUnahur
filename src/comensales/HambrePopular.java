package comensales;

import java.util.ArrayList;
import comidas.Comida;

public class HambrePopular extends Comensal {

	public HambrePopular(Double peso, ArrayList<Comida> comidasFavoritas) {
		super(peso, comidasFavoritas);
	}

	@Override
	public boolean leAgrada(Comida comida) {
		return comida.esAbundante() ? true : false;
	}

	@Override
	public boolean estaSatisfecho() {
		return condicion1PorcientoDelPesoPropio();
	}

	@Override
	public String toString() {
		return super.toString("Hambre Popular");
	}

}
