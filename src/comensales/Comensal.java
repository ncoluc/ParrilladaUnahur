package comensales;

import java.util.ArrayList;
import comidas.Comida;

public abstract class Comensal {

	private Double peso;
	private ArrayList<Comida> comidasIngeridas;
	private ArrayList<Comida> comidasFavoritas;

	public Comensal(Double peso, ArrayList<Comida> comidasFavoritas) {
		this.peso = peso;
		this.comidasIngeridas = new ArrayList<>();
		if (comidasFavoritas == null) {
			this.comidasFavoritas = new ArrayList<>();
		} else {
			this.comidasFavoritas = comidasFavoritas;
		}
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public ArrayList<Comida> getComidasIngeridas() {
		return comidasIngeridas;
	}

	public void setComidasIngeridas(ArrayList<Comida> comidasIngeridas) {
		this.comidasIngeridas = comidasIngeridas;
	}

	public ArrayList<Comida> getComidasFavoritas() {
		return comidasFavoritas;
	}

	public void setComidasFavoritas(ArrayList<Comida> comidasFavoritas) {
		this.comidasFavoritas = comidasFavoritas;
	}

	public void comer(Comida comida) {
		this.comidasIngeridas.add(comida);
	}
	
	public void agregarComidaFavorita(Comida comida) {
		this.comidasFavoritas.add(comida);
	}

	// Condicion igualitaria para todos los comensales.
	// El peso de todas las comidas ingeridas sea mayor o igual al 1% del propio
	// comensal
	protected boolean condicion1PorcientoDelPesoPropio() {
		Double pesoTotalDeComidasIngeridas = this.getComidasIngeridas().stream().mapToDouble(x -> x.getPeso()).sum();
		return pesoTotalDeComidasIngeridas >= (this.getPeso() * 0.01);
	}

	protected String toString(String tipoComensal) {
		String yaConsumio = getComidasIngeridas().isEmpty() ? "NO" : "SI";
		return "Comensal " + tipoComensal + " - peso: " + getPeso() + " - ya consumio alguna comida: " + yaConsumio;
	}

	public abstract boolean estaSatisfecho();

	public abstract boolean leAgrada(Comida comida);

}
