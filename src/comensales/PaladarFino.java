package comensales;

import java.util.ArrayList;
import comidas.Comida;

public class PaladarFino extends Comensal {

	private static final Integer valoracionMinima = 100;
	private static final Double pesoComidaMaximo = 300.0;
	private static final Double pesoComidaMinimo = 150.0;

	public PaladarFino(Double peso, ArrayList<Comida> comidasFavoritas) {
		super(peso, comidasFavoritas);
	}

	// Les agradan las comidas que pesan entre 150 y 300 gramos, y además tienen una
	// valoración mayor a 100.
	@Override
	public boolean leAgrada(Comida comida) {
		boolean leAgrada = false;
		if (comidaConPesoIdeal(comida) && (comida.getValoracion() > valoracionMinima)) {
			this.getComidasFavoritas().add(comida);
			leAgrada = true;
		}
		return leAgrada;
	}

	// La condición adicional para satisfacerse es que la cantidad de comidas
	// ingeridas sea par.
	@Override
	public boolean estaSatisfecho() {
		Boolean cantidadComidasIngeridasPar = this.getComidasIngeridas().size() % 2 == 0;
		return condicion1PorcientoDelPesoPropio() && cantidadComidasIngeridasPar;
	}

	private boolean comidaConPesoIdeal(Comida comida) {
		return comida.getPeso() <= pesoComidaMaximo && comida.getPeso() >= pesoComidaMinimo;
	}
	
	@Override
	public String toString() {
		return super.toString("Paladar Fino");
	}

}
