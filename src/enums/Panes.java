package enums;

import java.util.ArrayList;
import java.util.List;

public enum Panes {

	PAN_INDUSTRIAL("industrial", 0), PAN_CASERO("casero", 20), PAN_MASA_MADRE("masaMadre", 45);

	private Integer valorPan;
	private String tipoPan;

	Panes(String tipoPan, Integer valorPan) {
		this.tipoPan = tipoPan;
		this.valorPan = valorPan;
	}

	public Integer getValorPan() {
		return valorPan;
	}

	public String getTipoPan() {
		return tipoPan;
	}

	public void setTipoPan(String tipoPan) {
		this.tipoPan = tipoPan;
	}

	public void setValorPan(Integer valorPan) {
		this.valorPan = valorPan;
	}

	public static List<String> obtenerListaTipoPanes() {
		List<String> tiposDePanes = new ArrayList<String>();
		for (Panes pan : Panes.values()) {
			tiposDePanes.add(pan.getTipoPan());
		}
		return tiposDePanes;
	}

}
