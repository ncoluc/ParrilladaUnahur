package enums;

import java.util.ArrayList;
import java.util.List;

public enum Cortes {

	ASASDO("asado"), ENTRANIA("entrania"), CHORIZO("chorizo"), VACIO("vacio");

	private String nombreCorte;

	Cortes(String nombreCorte) {
		this.nombreCorte = nombreCorte;
	}

	public String getNombreCorte() {
		return nombreCorte;
	}

	public void setNombreCorte(String nombreCorte) {
		this.nombreCorte = nombreCorte;
	}

	public static List<String> obtenerListaDeNombresCortes() {
		List<String> listaCortes = new ArrayList<String>();
		for (Cortes corte : Cortes.values()) {
			listaCortes.add(corte.getNombreCorte());
		}
		return listaCortes;
	}

}
