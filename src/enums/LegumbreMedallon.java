package enums;

import java.util.ArrayList;
import java.util.List;

public enum LegumbreMedallon {

	MEDALLON_LENTEJAS("lentejas"), MEDALLON_GARBANZOS("garbanzos");

	private String legumbre;

	LegumbreMedallon(String legumbre) {
		this.legumbre = legumbre;
	}

	public String getLegumbre() {
		return legumbre;
	}

	public void setLegumbre(String legumbre) {
		this.legumbre = legumbre;
	}

	public static List<String> obtenerListaLegumbres() {
		List<String> listaLegumbres = new ArrayList<String>();
		for (LegumbreMedallon legumbre : LegumbreMedallon.values()) {
			listaLegumbres.add(legumbre.getLegumbre());
		}
		return listaLegumbres;
	}
}
