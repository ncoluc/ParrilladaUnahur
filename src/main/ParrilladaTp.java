package main;

import java.util.ArrayList;

import cocina.Cocina;
import comensales.Comensal;
import comidas.Comida;

public class ParrilladaTp {

	private Cocina cocina;
	private ArrayList<Comensal> comensales;

	public ParrilladaTp(Cocina cocina, ArrayList<Comensal> comensales) {
		this.cocina = cocina;
		this.comensales = comensales;
		if (comensales == null) {
			this.comensales = new ArrayList<>();
		} else {
			this.comensales = comensales;
		}
	}

	public Cocina getCocina() {
		return cocina;
	}

	public void setCocina(Cocina cocina) {
		this.cocina = cocina;
	}

	public ArrayList<Comensal> getComensales() {
		return comensales;
	}

	public void setComensales(ArrayList<Comensal> comensales) {
		this.comensales = comensales;
	}

	public void agregarComidaDisponible(Comida comida) {
		this.cocina.agregarComida(comida);
	}

	public void agregarComensal(Comensal comensal) {
		this.comensales.add(comensal);
	}

	public void listarComidasDisponiblesEnMenu() {
		System.out.println("Las comidas disponibles son las siguientes: ");
		for (Comida comida : cocina.getComidasDisponibles()) {
			System.out.println(comida.toString());
		}
	}

}