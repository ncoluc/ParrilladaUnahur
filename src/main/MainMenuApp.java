package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import cocina.Cocina;
import comensales.Comensal;
import comensales.HambrePopular;
import comensales.PaladarFino;
import comensales.Vegetariano;
import comidas.Comida;
import comidas.Corte;
import comidas.HamburguesaDeCarne;
import comidas.HamburguesaVegetariana;
import comidas.Parrillada;
import comidas.Provoleta;
import enums.Cortes;
import enums.LegumbreMedallon;
import enums.Panes;
import excepciones.ComensalSinComidasFavoritasExcepcion;
import excepciones.NoHayPlatoCarnivoEnCocinaExcepcion;
import excepciones.PlatoNoSeEncuentraDisponibleExcepcion;

import static enums.LegumbreMedallon.*;

public class MainMenuApp {

	private static final ParrilladaTp parrillaRosendo = new ParrilladaTp(null, null);

	// Scanner para utilizar la consola
	private static final Scanner scanner = new Scanner(System.in);

	public MainMenuApp() {
		
	}

	public static void main(String[] args) throws Exception {
		// Se crean algunos platos de comida y 2 comensales
		Cocina cocina = new Cocina(null);
		ArrayList<Comensal> comensales = new ArrayList<>();
		creacionDeComidaYComensales(cocina, comensales);
		parrillaRosendo.setCocina(cocina);
		parrillaRosendo.setComensales(comensales);

		System.out.println("La parrilla de Rosendo\n");
		String opcionElegida = "";
		while (!opcionElegida.equals("0")) {
			opcionElegida = menuPrincipalParrillada(parrillaRosendo, scanner);
		}
		System.out.println("Saliendo del sistema");
		System.out.println("Mañana sera otro día");
		System.exit(0);

	}

	private static String menuPrincipalParrillada(ParrilladaTp parrillaRosendo, Scanner scanner) throws Exception {
		System.out.println("Menu principal: ");
		System.out.println("###################################################");
		System.out.println("1- Agregar nueva comida\n" + "2- Hay buena oferta vegetariana\n"
				+ "3- Obtener plato fuerte carnivoro del dia\n" + "4- Obtener platos favoritos de un comensal\n"
				+ "5- Elegir un plato para un comensal\n" + "6- Agregar un nuevo comensal\n"
				+ "7- Obtener platos disponibles\n" + "8- Obtener comensales disponibles\n" + "\n\n"
				+ "0- Salir del sistema");

		String opcionElegida = controlDeOpcionElegida(scanner, 0, 8);
		switchMenu(opcionElegida);
		System.out.println("\n---------------------------------------------------\n");
		return opcionElegida;
	}

	private static void switchMenu(String opcionElegida) throws Exception {
		System.out.println("---------------------------------------------------");
		switch (opcionElegida) {
		case "1":
			agregarNuevaComidaMenu();
			break;
		case "2":
			getOfertaVegetarianaMenu();
			break;
		case "3":
			getPlatoFuerteCarnivoroMenu();
			break;
		case "4":
			getPlatosFavoritosDeUnComensalMenu();
			break;
		case "5":
			elegirPlatoParaComensalMenu();
			break;
		case "6":
			agregarNuevoComensalMenu();
			break;
		case "7":
			getPlatosDisponibles();
			break;
		case "8":
			getComensalesDisponibles();
			break;
		default:
			break;
		}
	}

	private static void getComensalesDisponibles() {
		System.out.println("8- Obtener comensales disponibles\n");
		ArrayList<Comensal> listaComensales = parrillaRosendo.getComensales();
		if (listaComensales.isEmpty()) {
			System.out.println("## No hay comensales todavia. Es muy temprano ##");
		} else {
			for (Comensal comensal : listaComensales) {
				System.out.println(comensal.toString());
			}
		}

	}

	private static void getPlatosDisponibles() {
		System.out.println("7- Obtener platos disponibles\n");
		List<Comida> listaComidas = parrillaRosendo.getCocina().getComidasDisponibles();
		if (listaComidas.isEmpty()) {
			System.out.println("## No hay platos disponibles todavia. Es muy temprano ##");
		} else {
			for (Comida comida : listaComidas) {
				System.out.println(comida.toString());
			}
		}

	}

	private static void agregarNuevaComidaMenu() throws Exception {
		System.out.println("1- Agregar nueva comida\n");
		System.out.println("Tipos de comidas: ");
		System.out.println("1- Parrillada");
		System.out.println("2- Hamburguesa");
		System.out.println("3- Provoleta");
		String opcionElegida = controlDeOpcionElegida(scanner, 1, 3);
		switch (opcionElegida) {
		case "1":
			agregarParrillada();
			break;
		case "2":
			agregarHamburguesa();
			break;
		case "3":
			agregarProvoleta();
			break;
		default:
			break;
		}

	}

	private static void getOfertaVegetarianaMenu() {
		System.out.println("2- Hay buena oferta vegetariana\n");
		String respuestaSatisfactoria = "## El dia de hoy contamos con una buena oferta vegetariana ##";
		String respuestaDesfavorable = "## Lamentablemente el dia de hoy NO contamos con una buena oferta vegetariana ##";
		String respuesta = parrillaRosendo.getCocina().buenaOfertaVegetariana() ? respuestaSatisfactoria
				: respuestaDesfavorable;
		System.out.println(respuesta);
	}

	private static void getPlatoFuerteCarnivoroMenu() {
		System.out.println("3- Obtener plato fuerte carnivoro del dia\n");
		String respuesta;
		try {
			Comida plato = parrillaRosendo.getCocina().obtenerPlatoFuerteCarnivoro();
			respuesta = "El plato fuerte carnivoro del dia de hoy es:\n" + plato.toString();
		} catch (NoHayPlatoCarnivoEnCocinaExcepcion e) {
			respuesta = "Lamentablemente el dia de hoy " + e.getMessage().toLowerCase();
		}
		System.out.println(respuesta);
	}

	private static void getPlatosFavoritosDeUnComensalMenu() {
		System.out.println("4- Obtener platos favoritos de un comensal\n");
		ArrayList<Comensal> comensales = getListaDeComensalesMenu();
		if (!comensales.isEmpty()) {
			System.out.println("\nSeleccione un comensal de la lista por favor");
			String opcionElegida = controlDeOpcionElegida(scanner, 1, comensales.size());
			System.out.println("Platos favoritoss del comensal de la mesa Nº " + opcionElegida + ":\n");
			ArrayList<Comida> listaComidasComensal = comensales.get(Integer.parseInt(opcionElegida) - 1)
					.getComidasFavoritas();
			if (listaComidasComensal.isEmpty()) {
				System.out.println("## El comensal no tiene ninguna comida favorita con nossotros ##");
			} else {

				for (Comida comida : listaComidasComensal) {
					System.out.println(comida.toString());
				}
			}
		} else {
			System.out.println("## Todavia no tenemos ningun comensal. Aun es temprano ##");
		}
	}

	private static void elegirPlatoParaComensalMenu() {
		System.out.println("5- Elegir un plato para un comensal\n");
		ArrayList<Comensal> comensales = getListaDeComensalesMenu();
		if (!comensales.isEmpty()) {
			System.out.println("Seleccione un comensal de la lista por favor");
			String opcionElegida = controlDeOpcionElegida(scanner, 1, comensales.size());
			System.out.println(
					"Se elegirá un plato de los disponibles para el comensasl de la mesa Nº " + opcionElegida + ":\n");
			Comensal comensal = parrillaRosendo.getComensales().get(Integer.parseInt(opcionElegida) - 1);
			try {
				parrillaRosendo.getCocina().elegirUnPlatoParaComensal(comensal);
				System.out.println("## El plato elegido fue del agrado del comensal, ya esta consumiendolo ##");
			} catch (ComensalSinComidasFavoritasExcepcion e) {
				System.out.println("## El comensal no tiene ninguna comida favorita con nosotros ##");
			} catch (PlatoNoSeEncuentraDisponibleExcepcion e) {
				System.out.println("## Lamentablemente no contamos con el plato favorito del cliente el dia de hoy ##");
			}
		} else {
			System.out.println("## Todavia no tenemos ningun comensal. Aun es temprano ##");
		}
	}

	private static void agregarNuevoComensalMenu() {
		System.out.println("6- Agregar un nuevo comensal\n");
		System.out.println("Tipos de comensales: ");
		System.out.println("1- Hambre Popular");
		System.out.println("2- Paladar Fino");
		System.out.println("3- Vegetariano");
		String opcionElegida = controlDeOpcionElegida(scanner, 1, 3);
		switch (opcionElegida) {
		case "1":
			agregarHambrePopular();
			break;
		case "2":
			agregarPaladarFino();
			break;
		case "3":
			agregarVegetariano();
			break;
		default:
			break;
		}
	}

	private static void agregarParrillada() throws Exception {
		System.out.println("\nSe va a crear una Parrillada");
		ArrayList<Corte> cortes = new ArrayList<>();
		boolean agregarMasCortes = true;
		do {
			System.out.println("Agregar un corte a la parrillada. Tiene las siguientes opciones diponibles: ");
			String nombreCorteElegido = listarIngredientesDeLaListaYSeleccionarUno(
					Cortes.obtenerListaDeNombresCortes());
			System.out.println("Por favor introduzca la calidad del corte. Un numero entero entre 1 y 10");
			// Pongo un limite de calidad 10, como perfecto.
			String calidad = controlDeOpcionElegida(scanner, 0, 10);
			System.out.println("Por favor introduzca el peso del corte en gramos con punto. Por ejemplo: 250.00\n"
					+ "PESO MINIMO: 50.00\n" + "PESO MAXIMO: 20000.00\n");
			Double pesoElegido = controlDeOpcionElegidaDouble(scanner, 50.00, 20000.00);
			Corte corte = new Corte(nombreCorteElegido, Integer.parseInt(calidad), pesoElegido);
			cortes.add(corte);
			System.out.println("Desea agregar mas cortes a la parrillada?");
			System.out.println("1- Si");
			System.out.println("2- No");
			agregarMasCortes = controlDeOpcionElegida(scanner, 1, 2).equals("1");
		} while (agregarMasCortes);

		Parrillada parrillada = new Parrillada(cortes);
		parrillaRosendo.agregarComidaDisponible(parrillada);
		mensajeNuevaComidaAgregada(parrillada);
	}

	private static void agregarHamburguesa() throws Exception {
		System.out.println("Se va a crear una Hamburguesa");
		System.out.println("Tipos de Hamburguesa: ");
		System.out.println("1- De carne");
		System.out.println("2- Vegetariana");
		String opcionElegida = controlDeOpcionElegida(scanner, 1, 2);
		switch (opcionElegida) {
		case "1":
			agregarHamburguesaDeCarne();
			break;
		case "2":
			agregarHamburguesaVegetariana();
			break;
		default:
			break;
		}
	}

	private static void mensajeNuevaComidaAgregada(Comida comida) {
		System.out.println("## Se agrego nueva comida ##");
		System.out.println(comida.toString());
	}

	private static void agregarHamburguesaDeCarne() throws Exception {
		HamburguesaDeCarne hamburguesa = new HamburguesaDeCarne(seleccioneTipoDePanHamburguesa());
		parrillaRosendo.agregarComidaDisponible(hamburguesa);
		mensajeNuevaComidaAgregada(hamburguesa);

	}

	private static void agregarHamburguesaVegetariana() throws Exception {
		HamburguesaVegetariana hamburguesa = new HamburguesaVegetariana(seleccioneTipoDeLegumbreHamburguesa(),
				seleccioneTipoDePanHamburguesa());
		parrillaRosendo.agregarComidaDisponible(hamburguesa);
		mensajeNuevaComidaAgregada(hamburguesa);
	}

	private static String seleccioneTipoDeLegumbreHamburguesa() {
		System.out.println("\nTipos de legumbre: ");
		return listarIngredientesDeLaListaYSeleccionarUno(LegumbreMedallon.obtenerListaLegumbres());
	}

	private static String seleccioneTipoDePanHamburguesa() {
		System.out.println("\nTipos de pan: ");
		return listarIngredientesDeLaListaYSeleccionarUno(Panes.obtenerListaTipoPanes());

	}

	private static String listarIngredientesDeLaListaYSeleccionarUno(List<String> lista) {
		for (int i = 0; i < lista.size(); i++) {
			System.out.println((i + 1) + "- " + lista.get(i));
		}
		String opcion = controlDeOpcionElegida(scanner, 1, lista.size());
		return lista.get(Integer.parseInt(opcion) - 1);
	}

	private static void agregarProvoleta() {
		System.out.println("Se va a crear una Provoleta");
		System.out.println(
				"Por favor introduzca a continuacion el peso de la provoleta en gramos con punto. Por ejemplo: 250.00\n"
						+ "PESO MINIMO: 50.00\n" + "PESO MAXIMO: 5000.00\n");
		Double pesoElegido = controlDeOpcionElegidaDouble(scanner, 50.00, 5000.00);
		System.out.println("Quiere que la provoleta tenga especias?");
		System.out.println("1- Si");
		System.out.println("2- No");
		boolean conEspecias = controlDeOpcionElegida(scanner, 1, 2).equals("1");
		Provoleta provoleta = new Provoleta(pesoElegido, conEspecias);
		parrillaRosendo.agregarComidaDisponible(provoleta);
		mensajeNuevaComidaAgregada(provoleta);
	}

	private static void agregarVegetariano() {
		Vegetariano comensal = new Vegetariano(0.0, null);
		agregarComensalEnParrilla(comensal, "Vegetariano");
	}

	private static void agregarPaladarFino() {
		PaladarFino comensal = new PaladarFino(0.0, null);
		agregarComensalEnParrilla(comensal, "Paladar Fino");
	}

	private static void agregarHambrePopular() {
		HambrePopular comensal = new HambrePopular(0.0, null);
		agregarComensalEnParrilla(comensal, "Hambre Popular");
	}

	private static void agregarComensalEnParrilla(Comensal comensal, String tipoComensal) {
		System.out.println("-- Nuevo Comensal " + tipoComensal + " --");
		System.out.println(
				"Por favor introduzca a continuacion el peso del comensal en gramos con punto. Por ejemplo: 75000.00\n"
						+ "PESO MINIMO: 20000.00");
		Double pesoElegido = controlDeOpcionElegidaDouble(scanner, 20000.00, 250000.00);
		comensal.setPeso(pesoElegido);
		parrillaRosendo.agregarComensal(comensal);
	}

	private static ArrayList<Comensal> getListaDeComensalesMenu() {
		ArrayList<Comensal> comensales = parrillaRosendo.getComensales();
		System.out.println("Lista de comensales:\n");
		if (comensales.isEmpty()) {
			System.out.println("Todavia no tenemos ningun comensal. Aun es temprano");
		} else {
			int nroMesa = 1;
			for (Comensal comensal : comensales) {
				System.out.println("Mesa Nº" + nroMesa + " - " + comensal.toString());
				nroMesa++;
			}
		}
		return comensales;
	}

	private static String controlDeOpcionElegida(Scanner scanner, int minimo, int maximo) {
		System.out.println("Introduzca un valor por favor: ");
		String opcion = scanner.nextLine();
		if (opcion.equals("")) {
			System.out.println("X - Dato ingresado incorrecto - X");
			System.out.println("Por favor a continuacion ingrese un numero entre " + minimo + " y " + maximo);
			return controlDeOpcionElegida(scanner, minimo, maximo);
		}
		if (opcion.matches("[0-9]*")) {
			Integer opcionInteger = Integer.parseInt(opcion);
			if (opcionInteger <= maximo && opcionInteger >= minimo) {
				return opcion;
			} else {
				System.out.println("X - Dato ingresado incorrecto - X");
				System.out.println("Por favor a continuacion ingrese un numero entre " + minimo + " y " + maximo);
				return controlDeOpcionElegida(scanner, minimo, maximo);
			}
		} else {
			System.out.println("X - Dato ingresado incorrecto - X");
			System.out.println("Por favor a continuacion ingrese un numero entre " + minimo + " y " + maximo);
			return controlDeOpcionElegida(scanner, minimo, maximo);
		}
	}

	private static Double controlDeOpcionElegidaDouble(Scanner scanner, Double minimo, Double maximo) {
		System.out.println("Introduzca un valor por favor: ");
		String opcion = scanner.nextLine();
		if (opcion.equals("")) {
			System.out.println("X - Dato ingresado incorrecto - X");
			System.out.println("Por favor a continuacion ingrese un numero entre " + minimo + " y " + maximo);
			return controlDeOpcionElegidaDouble(scanner, minimo, maximo);
		}
		if (opcion.matches("[0-9]*(.[0-9][0-9]?)?")) {
			Double opcionDouble = Double.parseDouble(opcion);
			if (opcionDouble <= maximo && opcionDouble >= minimo) {
				return opcionDouble;
			} else {
				System.out.println("X - Dato ingresado incorrecto - X");
				System.out.println("Por favor a continuacion ingrese un numero entre " + minimo + " y " + maximo);
				return controlDeOpcionElegidaDouble(scanner, minimo, maximo);
			}
		} else {
			System.out.println("X - Dato ingresado incorrecto - X");
			System.out.println("Por favor a continuacion ingrese un numero entre " + minimo + " y " + maximo);
			return controlDeOpcionElegidaDouble(scanner, minimo, maximo);
		}
	}

	private static void creacionDeComidaYComensales(Cocina cocina, ArrayList<Comensal> comensales) throws Exception {
		Corte asado = new Corte("asado", 10, 250.0);
		Corte entrania = new Corte("entrania", 7, 200.0);
		Corte chori = new Corte("chorizo", 8, 50.0);
		Corte vacio = new Corte("vacio", 8, 1000.0);
		ArrayList<Corte> cortes = new ArrayList<Corte>();
		ArrayList<Corte> cortes2 = new ArrayList<Corte>();
		cortes.add(asado);
		cortes.add(entrania);
		cortes.add(chori);
		cortes2.add(entrania);
		cortes2.add(vacio);
		Parrillada parrillada1 = new Parrillada(cortes);
		Parrillada parrillada2 = new Parrillada(cortes2);

		HamburguesaVegetariana hamburguesa1 = new HamburguesaVegetariana(MEDALLON_GARBANZOS.getLegumbre(),
				Panes.PAN_MASA_MADRE.getTipoPan());
		HamburguesaVegetariana hamburguesa2 = new HamburguesaVegetariana(MEDALLON_GARBANZOS.getLegumbre(),
				Panes.PAN_INDUSTRIAL.getTipoPan());
		HamburguesaVegetariana hamburguesa3 = new HamburguesaVegetariana(MEDALLON_LENTEJAS.getLegumbre(),
				Panes.PAN_CASERO.getTipoPan());
		HamburguesaDeCarne hamburguesa4 = new HamburguesaDeCarne(Panes.PAN_MASA_MADRE.getTipoPan());

		Provoleta provoleta1 = new Provoleta(190.0, true);
		Provoleta provoleta2 = new Provoleta(300.0, false);
		Provoleta provoleta3 = new Provoleta(260.0, false);
		Provoleta provoleta4 = new Provoleta(260.0, false);

		cocina.agregarComida(parrillada1);
		cocina.agregarComida(parrillada2);
		cocina.agregarComida(hamburguesa1);
		cocina.agregarComida(hamburguesa2);
		cocina.agregarComida(hamburguesa3);
		cocina.agregarComida(hamburguesa4);
		cocina.agregarComida(provoleta1);
		cocina.agregarComida(provoleta2);
		cocina.agregarComida(provoleta3);
		cocina.agregarComida(provoleta4);

		HambrePopular comensal1 = new HambrePopular(85000.0, null);
		Vegetariano comensal2 = new Vegetariano(75000.0, null);

		comensal1.agregarComidaFavorita(parrillada1);
		comensal1.agregarComidaFavorita(provoleta4);
		comensal1.agregarComidaFavorita(parrillada2);
		comensal1.agregarComidaFavorita(hamburguesa4);
		comensal1.agregarComidaFavorita(hamburguesa3);

		comensales.add(comensal1);
		comensales.add(comensal2);
	}

}
