package excepciones;

@SuppressWarnings("serial")
public class TipoDePanInvalidoExcepcion extends Exception {

	public TipoDePanInvalidoExcepcion() {
		super("El tipo de pan es NULO o no es valido");
	}

}
