package excepciones;

@SuppressWarnings("serial")
public class PlatoNoLeAgradaAlComensalExcepcion extends Exception {

	public PlatoNoLeAgradaAlComensalExcepcion() {
		super("El plato elegido no le agrada al comensal");
	}

}
