package excepciones;

@SuppressWarnings("serial")
public class ComensalSinComidasFavoritasExcepcion extends Exception {

	public ComensalSinComidasFavoritasExcepcion() {
		super("El comensal no tiene comidas favoritas");
	}

}
