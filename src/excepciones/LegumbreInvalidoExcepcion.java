package excepciones;

@SuppressWarnings("serial")
public class LegumbreInvalidoExcepcion extends Exception {

	public LegumbreInvalidoExcepcion() {
		super("La legumbre es NULO o no es valida");
	}

}
