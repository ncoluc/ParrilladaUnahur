package excepciones;

@SuppressWarnings("serial")
public class ParametroIngresadoInvalidoExcepcion extends Exception {

	public ParametroIngresadoInvalidoExcepcion() {
		super("Parametro ingresado invalido o nulo");
	}

}
