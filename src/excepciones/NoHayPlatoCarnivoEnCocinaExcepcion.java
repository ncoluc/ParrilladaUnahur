package excepciones;

@SuppressWarnings("serial")
public class NoHayPlatoCarnivoEnCocinaExcepcion extends Exception {

	public NoHayPlatoCarnivoEnCocinaExcepcion() {
		super("No hay ningun plato carnivoro");
	}
}
