package excepciones;

@SuppressWarnings("serial")
public class PlatoNoSeEncuentraDisponibleExcepcion extends Exception {

	public PlatoNoSeEncuentraDisponibleExcepcion() {
		super("El plato no se encuentra disponible");
	}

}
