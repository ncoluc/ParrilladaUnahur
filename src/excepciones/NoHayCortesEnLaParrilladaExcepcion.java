package excepciones;

@SuppressWarnings("serial")
public class NoHayCortesEnLaParrilladaExcepcion extends Exception {

	public NoHayCortesEnLaParrilladaExcepcion() {
		super("No hay ningun corte en la parrillada");
	}

}
