package tests;

import org.junit.Test;

import comensales.HambrePopular;
import comensales.PaladarFino;
import comensales.Vegetariano;
import comidas.Corte;
import comidas.HamburguesaDeCarne;
import comidas.HamburguesaVegetariana;
import comidas.Parrillada;
import comidas.Provoleta;
import junit.framework.TestCase;

import java.util.ArrayList;

public class TestComensales extends TestCase{

	@Test
	public void testVegetarianoSatisfechoOK() throws Exception {
		System.out.println("Test: ");
		System.out.println("Un vegetariano de peso 68500 gramos come una provoleta de 190 gramos con especias "
				+ "y dos Hamburguesas Vegetarianas (250 grs cada una) con pan de masaMadre y de garbanzos como legumbre. Debe estar satisfecho");

		Vegetariano vegetariano = new Vegetariano(68500.0, null);

		Provoleta provoleta = new Provoleta(190.0, true);
		HamburguesaVegetariana hamburguesa1 = new HamburguesaVegetariana("garbanzos", "masaMadre");
		HamburguesaVegetariana Hamburguesa2 = new HamburguesaVegetariana("garbanzos", "masaMadre");

		vegetariano.comer(provoleta);
		vegetariano.comer(hamburguesa1);
		vegetariano.comer(Hamburguesa2);

		assertEquals(true, vegetariano.estaSatisfecho());
	}

	@Test
	public void testComensalPopularInsatisfechoOK() throws Exception {
		System.out.println("Test: ");
		System.out.println("Un comensal popular de peso 85000 gramos come una parrillada que incluye un asado, "
				+ "de calidad 10 y peso 250 gramos, una entraña, de calidad 7 y peso 200 gramos y un chorizo, de calidad 8 y peso 50 gramos no esta satisfecho");

		HambrePopular comensalPopular = new HambrePopular(85000.0, null);

		Corte asado = new Corte("asado", 10, 250.0);
		Corte entrania = new Corte("entrania", 7, 200.0);
		Corte chorizo = new Corte("chorizo", 8, 50.0);

		ArrayList<Corte> cortes = new ArrayList<Corte>();
		cortes.add(asado);
		cortes.add(entrania);
		cortes.add(chorizo);

		Parrillada parrillada = new Parrillada(cortes);

		comensalPopular.comer(parrillada);

		assertEquals(false, comensalPopular.estaSatisfecho());
	}

	@Test
	public void testComensalPaladarFinoLeAgradaHamburguesaOK() throws Exception {
		System.out.println("Test: ");
		System.out.println(
				"Un comensal de paladar fino le agrada comer una Hamburguesa de carne de 250 gramos con pan de masa madre.");

		PaladarFino comensalFino = new PaladarFino(80000.0, null);
		HamburguesaDeCarne hamburguesa = new HamburguesaDeCarne("masaMadre");

		assertEquals(true, comensalFino.leAgrada(hamburguesa));
	}

	@Test
	public void testComensalPaladarFinoNoLeAgradaHamburguesaOK() throws Exception {
		System.out.println("Test: ");
		System.out.println(
				"Un comensal de paladar fino NO agrada comer una Hamburguesa de carne de 250 gramos con pan casero");

		PaladarFino comensalFino = new PaladarFino(75000.0, null);
		HamburguesaDeCarne hamburguesa = new HamburguesaDeCarne("casero");

		assertEquals(false, comensalFino.leAgrada(hamburguesa));
	}

}
