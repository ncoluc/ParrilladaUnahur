package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

import cocina.Cocina;
import comensales.HambrePopular;
import comidas.Comida;
import comidas.Corte;
import comidas.HamburguesaDeCarne;
import comidas.HamburguesaVegetariana;
import comidas.Parrillada;
import comidas.Provoleta;
import excepciones.ComensalSinComidasFavoritasExcepcion;
import excepciones.NoHayPlatoCarnivoEnCocinaExcepcion;
import excepciones.PlatoNoSeEncuentraDisponibleExcepcion;
import junit.framework.TestCase;

public class TestCocina extends TestCase {

	@Test
	public void testCocinaConBuenaOfertaVegetarianaOK() throws Exception {
		System.out.println("Test: ");
		System.out.println(
				"Cocina con una buena oferta vegetariana. 3 comidas carnivoras y 1 vegetariana. Resultado esperado true");

		Cocina cocina = new Cocina(null);
		HamburguesaDeCarne hamburguesa1 = new HamburguesaDeCarne("masaMadre");
		HamburguesaDeCarne hamburguesa2 = new HamburguesaDeCarne("industrial");
		HamburguesaVegetariana hamburguesa3 = new HamburguesaVegetariana("garbanzos", "masaMadre");
		Provoleta provoleta = new Provoleta(150.0, true);

		cocina.agregarComida(hamburguesa1);
		cocina.agregarComida(hamburguesa2);
		cocina.agregarComida(hamburguesa3);
		cocina.agregarComida(provoleta);

		assertEquals(true, cocina.buenaOfertaVegetariana());
	}

	@Test
	public void testObtenerPlatoFuerteCarnivoroCorretoOK() throws Exception {
		System.out.println("Test: ");
		System.out.println(
				"Validacion obtener plato fuerte carnivoro de cocina con 5 platos. 2 carnivoros y 3 vegetariano. Resultado esperado: Devuelve la parrillada por su valoracion mayor");

		Cocina cocina = new Cocina(null);

		HamburguesaDeCarne hamburguesa1 = new HamburguesaDeCarne("masaMadre");
		HamburguesaVegetariana hamburguesa2 = new HamburguesaVegetariana("garbanzos", "industrial");
		HamburguesaVegetariana hamburguesa3 = new HamburguesaVegetariana("garbanzos", "masaMadre");
		Provoleta provoleta = new Provoleta(150.0, false);

		Corte asado = new Corte("asado", 10, 250.0);
		Corte entrania = new Corte("entrania", 7, 200.0);
		Corte chori = new Corte("chorizo", 8, 50.0);
		ArrayList<Corte> cortes = new ArrayList<Corte>();
		cortes.add(asado);
		cortes.add(entrania);
		cortes.add(chori);
		Parrillada parrillada = new Parrillada(cortes);

		cocina.agregarComida(hamburguesa1);
		cocina.agregarComida(hamburguesa2);
		cocina.agregarComida(hamburguesa3);
		cocina.agregarComida(provoleta);
		cocina.agregarComida(parrillada);

		assertEquals(parrillada, cocina.obtenerPlatoFuerteCarnivoro());
	}

	@Test
	public void testObtenerPlatoFuerteCarnivoroConCocinaConSoloPlatosVegetarianosSeEsperaExcepcionOK() {
		System.out.println("Test: ");
		System.out.println(
				"Validacion obtener plato fuerte carnivoro de cocina sin platos carnivoros. Resultado esperado: NoHayPlatoCarnivoEnCocinaExcepcion.class");

		Cocina cocina = new Cocina(null);

		Provoleta provoleta1 = new Provoleta(150.0, false);
		Provoleta provoleta2 = new Provoleta(200.0, false);
		Provoleta provoleta3 = new Provoleta(130.0, false);

		cocina.agregarComida(provoleta1);
		cocina.agregarComida(provoleta2);
		cocina.agregarComida(provoleta3);

		assertThrows(NoHayPlatoCarnivoEnCocinaExcepcion.class, () -> cocina.obtenerPlatoFuerteCarnivoro());
	}

	@Test
	public void testElegirUnPlatoParaComensalSinComidasFavoritasSeEsperaExcepcionOK() {
		System.out.println("Test: ");
		System.out.println(
				"Validacion elegir un plato para comensal sin comidas favoritas. Resultado esperado: ComensalSinComidasFavoritasExcepcion.class");

		Cocina cocina = new Cocina(null);
		Provoleta provoleta1 = new Provoleta(150.0, false);
		cocina.agregarComida(provoleta1);

		HambrePopular comensal = new HambrePopular(75000.0, null);

		assertThrows(ComensalSinComidasFavoritasExcepcion.class, () -> cocina.elegirUnPlatoParaComensal(comensal));
	}

	@Test
	public void testElegirUnPlatoParaComensalDeSusComidasFavoritasPeroElPlatoNoDisponibleEnLaCocinaSeEsperaExcepcionOK() {
		System.out.println("Test: ");
		System.out.println(
				"Validacion elegir un plato para comensal de sus comidas favoritas, pero el plato no esta disponible en la cocina. "
						+ "Resultado esperado: PlatoNoSeEncuentraDisponibleExcepcion.class");

		Cocina cocina = new Cocina(null);
		Provoleta provoleta1 = new Provoleta(150.0, false);
		Provoleta provoleta2 = new Provoleta(260.0, true);
		cocina.agregarComida(provoleta1);

		ArrayList<Comida> comidas = new ArrayList<Comida>();
		comidas.add(provoleta2);

		HambrePopular comensal = new HambrePopular(75000.0, comidas);

		assertThrows(PlatoNoSeEncuentraDisponibleExcepcion.class, () -> cocina.elegirUnPlatoParaComensal(comensal));
	}

	@Test
	public void testLuegoDeEligirPlatoParaComensalElPlatoNoSeEncuentraEntreLosDisponiblesDeLaCocinaResultadoOK()
			throws Exception {
		System.out.println("Test: ");
		System.out.println(
				"Validacion luego de elegir un plato para comensal el plato no se encuentra entre los disponibles de la cocina. "
						+ "Resultado esperado: El plato consumido por el comensal no esta mas disponible en la cocina");

		Cocina cocina = new Cocina(null);
		Provoleta provoleta1 = new Provoleta(150.0, false);
		Provoleta provoleta2 = new Provoleta(260.0, true);
		HamburguesaDeCarne hamburguesa = new HamburguesaDeCarne("industrial");
		cocina.agregarComida(provoleta1);
		cocina.agregarComida(provoleta2);
		cocina.agregarComida(hamburguesa);

		ArrayList<Comida> comidas = new ArrayList<Comida>();
		comidas.add(provoleta2);
		comidas.add(hamburguesa);

		HambrePopular comensal = new HambrePopular(75000.0, comidas);

		assertTrue(cocina.getComidasDisponibles().contains(provoleta1));
		assertTrue(cocina.getComidasDisponibles().contains(provoleta2));
		assertTrue(cocina.getComidasDisponibles().contains(hamburguesa));

		cocina.elegirUnPlatoParaComensal(comensal);

		// Los asserts son con la provoleta porque sabemos que el plato elegido para
		// consumir es el primero de la lista de comidas favoritas del comensal
		assertFalse(cocina.getComidasDisponibles().contains(provoleta2));
		assertTrue(comensal.getComidasIngeridas().contains(provoleta2));
	}

}
