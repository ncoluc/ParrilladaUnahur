package tests;

import java.util.ArrayList;

import org.junit.Test;

import comidas.Corte;
import comidas.HamburguesaDeCarne;
import comidas.HamburguesaVegetariana;
import comidas.Parrillada;
import comidas.Provoleta;
import junit.framework.TestCase;

public class TestComidas extends TestCase {

	@Test
	public void testValoracionHamburguesaCarneConPanCaserosResultadoOK() throws Exception {
		System.out.println("Test: ");
		System.out.println("Valoración de la Hambruguesa de Carne con pan casero. El resultado será: 80 (60 + 20)");

		Integer resultadoEsperado = 80;
		HamburguesaDeCarne hamburguesa = new HamburguesaDeCarne("casero");
		assertEquals(resultadoEsperado, hamburguesa.getValoracion());
	}

	@Test
	public void testValoracionHamburguesaVegetarianaConPanMasaMadreResultadoOK() throws Exception {
		System.out.println("Test: ");
		System.out
				.println("Valoración de la Hambruguesa Vegetariana, con pan de masaMadre y de garbanzos como legumbre. "
						+ "El resultado será: 122 (60 + 45 + 17)");

		Integer resultadoEsperado = 122;
		HamburguesaVegetariana hamburguesa = new HamburguesaVegetariana("garbanzos", "masaMadre");
		assertEquals(resultadoEsperado, hamburguesa.getValoracion());
	}

	@Test
	public void testProvoletaDe190gConEspeciasEsEspecialResultadoOK() {
		System.out.println("Test: ");
		System.out.println("Provoleta, de peso 190 y con especias. El resultado de ser especial es: true");

		Provoleta provo = new Provoleta(190.0, true);
		assertEquals(true, provo.isEspecial());
	}

	@Test
	public void testValoracionParrilladaConAsadoEntraniaYChoriResultadoOK() throws Exception {
		System.out.println("Test: ");
		System.out.println(
				"Valoracion de una parrillada que incluya un asado de calidad 10 y 250g, una entrania de calidad 7 y peso 200g y un chori de calidad 8 y 50g. "
						+ "El resultado será: 147");

		Integer resultadoEsperado = 147;

		Corte asado = new Corte("asado", 10, 250.0);
		Corte entrania = new Corte("entrania", 7, 200.0);
		Corte chori = new Corte("chorizo", 8, 50.0);

		ArrayList<Corte> cortes = new ArrayList<Corte>();
		cortes.add(asado);
		cortes.add(entrania);
		cortes.add(chori);

		Parrillada parrillada = new Parrillada(cortes);
		assertEquals(resultadoEsperado, parrillada.getValoracion());
	}

}
