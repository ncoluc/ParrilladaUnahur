package tests;

import junit.textui.TestRunner;

public class TestParrilladaTpRunner{

	public static void main(String[] args) {

		System.out.println("Tests de Cocina: \n");
		TestRunner.run(TestCocina.class);
		System.out.println("#############################################################################\n\n");

		System.out.println("Tests de Comensales: \n");
		TestRunner.run(TestComensales.class);
		System.out.println("#############################################################################\n\n");

		System.out.println("Tests de Comidas: \n");
		TestRunner.run(TestComidas.class);
		System.out.println("#############################################################################\n\n");
	}
}