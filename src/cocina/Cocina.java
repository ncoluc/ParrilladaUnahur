package cocina;

import comidas.Comida;
import excepciones.ComensalSinComidasFavoritasExcepcion;
import excepciones.NoHayPlatoCarnivoEnCocinaExcepcion;
import excepciones.PlatoNoSeEncuentraDisponibleExcepcion;
import comensales.Comensal;

import java.util.ArrayList;
import java.util.List;

public class Cocina {

	private ArrayList<Comida> comidasDisponibles;

	public Cocina(ArrayList<Comida> comidasPreparadas) {
		if (comidasPreparadas == null) {
			this.comidasDisponibles = new ArrayList<>();
		} else {
			this.comidasDisponibles = comidasPreparadas;
		}
	}

	public List<Comida> getComidasDisponibles() {
		return comidasDisponibles;
	}

	public void setComidasDisponibles(ArrayList<Comida> comidasDisponibles) {
		this.comidasDisponibles = comidasDisponibles;
	}

	public void agregarComida(Comida comida) {
		this.comidasDisponibles.add(comida);
	}

	public boolean buenaOfertaVegetariana() {
		int cantidadComidasCarnivoras = 0;
		int cantidadComidasVegetarianas = 0;

		for (Comida comida : this.getComidasDisponibles()) {
			if (comida.isAptoVegetariano()) {
				cantidadComidasVegetarianas += 1;
			} else {
				cantidadComidasCarnivoras += 1;
			}
		}
		return (cantidadComidasCarnivoras - cantidadComidasVegetarianas) <= 2;
	}

	public Comida obtenerPlatoFuerteCarnivoro() throws NoHayPlatoCarnivoEnCocinaExcepcion {
		Comida platoFuerte = this.getComidasDisponibles().stream().filter(x -> x.isAptoVegetariano() == false)
				.max((o1, o2) -> o1.getValoracion().compareTo(o2.getValoracion()))
				.orElseThrow(() -> new NoHayPlatoCarnivoEnCocinaExcepcion());
		return platoFuerte;
	}

	public List<Comida> obtenerPlatosFavoritosDeUnComensal(Comensal comensal) {
		return comensal.getComidasFavoritas();
	}

	public Comensal elegirUnPlatoParaComensal(Comensal comensal)
			throws ComensalSinComidasFavoritasExcepcion, PlatoNoSeEncuentraDisponibleExcepcion {
		if (comensal.getComidasFavoritas().isEmpty()) {
			throw new ComensalSinComidasFavoritasExcepcion();
		}
		Comida platoElegido = comensal.getComidasFavoritas().get(0);

		if (!comidasDisponibles.contains(platoElegido)) {
			throw new PlatoNoSeEncuentraDisponibleExcepcion();
		}

		comensal.comer(platoElegido);
		this.comidasDisponibles.remove(platoElegido);
		return comensal;
	}
}
