package comidas;

import excepciones.ParametroIngresadoInvalidoExcepcion;

public class Corte {

	private String nombre;
	private Integer calidad;
	private Double peso;

	public Corte(String nombre, Integer calidad, Double peso) throws Exception {
		this.setNombre(nombre);
		this.setPeso(peso);
		// Si la calidad es 0 se arruina el calculo de la valoracion de la parrillada
		// donde estaria el corte. Por defecto pongo como minimo una calidad = 1;
		Integer calidadCorrecta = calidad < 1 ? 1 : calidad;
		this.setCalidad(calidadCorrecta);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) throws ParametroIngresadoInvalidoExcepcion {
		this.nombre = nombre;
		validarNombre();
	}

	public Integer getCalidad() {
		return calidad;
	}

	public void setCalidad(Integer calidad) {
		this.calidad = calidad;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) throws ParametroIngresadoInvalidoExcepcion {
		this.peso = peso;
		validarPeso();
	}

	private void validarNombre() throws ParametroIngresadoInvalidoExcepcion {
		if (this.getNombre() == null || this.getNombre().equals("")) {
			throw new ParametroIngresadoInvalidoExcepcion();
		}
	}

	private void validarPeso() throws ParametroIngresadoInvalidoExcepcion {
		if (this.getPeso() == null || this.getPeso() <= (0.0)) {
			throw new ParametroIngresadoInvalidoExcepcion();
		}
	}

}
