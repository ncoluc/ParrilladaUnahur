package comidas;

public abstract class Comida {
	private Double peso;
	private boolean aptoVegetariano;
	private Integer valoracion;

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public boolean isAptoVegetariano() {
		return aptoVegetariano;
	}

	public void setAptoVegetariano(boolean aptoVegetariano) {
		this.aptoVegetariano = aptoVegetariano;
	}

	public Integer getValoracion() {
		return valoracion;
	}

	public void setValoracion(Integer valoracion) {
		this.valoracion = valoracion;
	}

	public boolean esAbundante() {
		return this.peso > 250;
	}

	public abstract void calcularValoracion() throws Exception;
}
