package comidas;

public class Provoleta extends Comida {

	private static Integer valoracionProvoletaEspecial = 120;
	private static Integer valoracionProvoletaComun = 80;
	private boolean tieneEspecias;
	private boolean especial;

	public Provoleta(Double peso, boolean tieneEspecias) {
		this.setPeso(peso);
		this.setTieneEspecias(tieneEspecias);
		this.setAptoVegetariano(!tieneEspecias);
		this.setEspecial(tieneEspecias || this.esAbundante());
		calcularValoracion();
	}

	public boolean isTieneEspecias() {
		return tieneEspecias;
	}

	public void setTieneEspecias(boolean tieneEspecias) {
		this.tieneEspecias = tieneEspecias;
	}

	public boolean isEspecial() {
		return especial;
	}

	private void setEspecial(boolean especial) {
		this.especial = especial;
	}

	@Override
	public void calcularValoracion() {
		Integer valorProvoleta = this.isEspecial() ? valoracionProvoletaEspecial : valoracionProvoletaComun;
		this.setValoracion(valorProvoleta);
	}

	public String toString() {
		String tieneEspeciasSioNo = isTieneEspecias() ? "SI" : "NO";
		String esEspecial = isEspecial() ? "SI" : "NO";
		String esVegetariana = isAptoVegetariano() ? "SI" : "NO";
		String provoleta = "Provoleta - valoracion: " + getValoracion() + " - especial: " + esEspecial + " - especias: "
				+ tieneEspeciasSioNo + " - apto vegetariano: " + esVegetariana + " - peso: " + getPeso() + "\n";
		return provoleta;
	}
}
