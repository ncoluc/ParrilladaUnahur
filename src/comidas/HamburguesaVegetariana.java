package comidas;

import enums.LegumbreMedallon;
import excepciones.LegumbreInvalidoExcepcion;

public class HamburguesaVegetariana extends Hamburguesa {

	private String legumbreMedallon;

	public HamburguesaVegetariana(String legumbreMedallon, String tipoDePan) throws Exception {
		super(tipoDePan);
		this.setLegumbreMedallon(legumbreMedallon);
		validarLegumbreValida();
		this.setAptoVegetariano(true);
		calcularValoracion();
	}

	public String getLegumbreMedallon() {
		return legumbreMedallon;
	}

	public void setLegumbreMedallon(String legumbreMedallon) {
		this.legumbreMedallon = legumbreMedallon;
	}

	@Override
	public void calcularValoracion() throws Exception {
		this.calcularValoracionPorTipoDePan();
		calcularValoracionExtraPorTipoDeMedallon();
	}

	public void calcularValoracionExtraPorTipoDeMedallon() {
		Integer calculoValoracionExtra = this.getLegumbreMedallon().length() * 2;
		Integer valoracionExtraPorMedallon = (calculoValoracionExtra < 17) ? calculoValoracionExtra : 17;
		this.setValoracion(this.getValoracion() + valoracionExtraPorMedallon);
	}

	private void validarLegumbreValida() throws Exception {
		if (this.getLegumbreMedallon() == null || this.getLegumbreMedallon().equals("")
				|| !LegumbreMedallon.obtenerListaLegumbres().contains(this.getLegumbreMedallon())) {
			throw new LegumbreInvalidoExcepcion();
		}
	}

	@Override
	public String toString() {
		String hamburguesa = "Hamburguesa Vegetariana - valoracion: " + getValoracion() + " - pan: " + getTipoDePan() +  " - legumbre: " + getLegumbreMedallon() + " - peso: " + getPeso() + "\n";
		return hamburguesa;
	}
}
