package comidas;

public class HamburguesaDeCarne extends Hamburguesa {

	public HamburguesaDeCarne(String tipoDePan) throws Exception {
		super(tipoDePan);
		this.setAptoVegetariano(false);
		calcularValoracionPorTipoDePan();
	}

	@Override
	public void calcularValoracion() throws Exception {
		calcularValoracionPorTipoDePan();
	}

	@Override
	public String toString() {
		String hamburguesa = "Hamburguesa de Carne - valoracion: " + getValoracion() + " - pan: " + getTipoDePan() + " - peso: " + getPeso() + "\n";
		return hamburguesa;
	}
}
