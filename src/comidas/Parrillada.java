package comidas;

import java.util.ArrayList;

import excepciones.NoHayCortesEnLaParrilladaExcepcion;

public class Parrillada extends Comida {

	private ArrayList<Corte> cortes;

	public Parrillada(ArrayList<Corte> cortes) throws NoHayCortesEnLaParrilladaExcepcion {
		if (cortes == null) {
			this.cortes = new ArrayList<>();
		} else {
			this.cortes = cortes;
		}
		this.setAptoVegetariano(false);
		calcularPeso();
		calcularValoracion();
	}

	public ArrayList<Corte> getCortes() {
		return cortes;
	}

	public void setCortes(ArrayList<Corte> cortes) {
		this.cortes = cortes;
	}

	public void agregarCorte(Corte corte) {
		this.cortes.add(corte);
	}

	@Override
	public void calcularValoracion() throws NoHayCortesEnLaParrilladaExcepcion {
		Integer valoracion = (15 * obtenerMayorCalidadEntreLosCortes()) - this.getCortes().size();
		Integer valoracionFinal = valoracion < 1 ? 1 : valoracion;
		this.setValoracion(valoracionFinal);
	}

	public void calcularPeso() {
		Double pesoTotalParrillada = cortes.stream().mapToDouble(x -> x.getPeso()).sum();
		this.setPeso(pesoTotalParrillada);
	}

	private Integer obtenerMayorCalidadEntreLosCortes() throws NoHayCortesEnLaParrilladaExcepcion {
		Corte corteMayorCalidad = getCortes().stream().max((c1, c2) -> c1.getCalidad().compareTo(c2.getCalidad()))
				.orElseThrow(() -> new NoHayCortesEnLaParrilladaExcepcion());
		return corteMayorCalidad.getCalidad();
	}

	public String toString() {
		StringBuilder listaDeCortes = new StringBuilder();
		for (Corte corte : cortes) {
			listaDeCortes.append("        # " + corte.getNombre() + " - calidad: " + corte.getCalidad() + " - peso: "
					+ corte.getPeso() + "\n");
		}
		String parrillada = "Parrillada - valoracion: " + getValoracion() + " - peso: " + getPeso() + "\n";
		return parrillada + listaDeCortes;
	}
}
