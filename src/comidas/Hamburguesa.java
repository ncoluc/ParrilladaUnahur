package comidas;

import java.util.Arrays;
import java.util.List;

import enums.Panes;
import excepciones.TipoDePanInvalidoExcepcion;

public abstract class Hamburguesa extends Comida {

	private static final Integer valoracionDefaultBase = 60;
	private static final Double pesoDefaultHamburguesa = 250.0;
	private String tipoDePan;

	public Hamburguesa(String tipoDePan) throws TipoDePanInvalidoExcepcion {
		this.setTipoDePan(tipoDePan);
		validarPanValido();
		this.setPeso(pesoDefaultHamburguesa);
		this.setValoracion(valoracionDefaultBase);
	}

	private void validarPanValido() throws TipoDePanInvalidoExcepcion {
		if (this.getTipoDePan() == null || this.getTipoDePan().equals("")
				|| !Panes.obtenerListaTipoPanes().contains(this.getTipoDePan())) {
			throw new TipoDePanInvalidoExcepcion();
		}
	}

	public String getTipoDePan() {
		return tipoDePan;
	}

	public void setTipoDePan(String tipoDePan) throws TipoDePanInvalidoExcepcion {
		this.tipoDePan = tipoDePan;
		validarPanValido();
	}

	public void calcularValoracionPorTipoDePan() {
		List<Panes> panes = Arrays.asList(Panes.values());
		Panes pan = panes.stream().filter(x -> x.getTipoPan().equals(this.getTipoDePan())).findFirst().get();
		this.setValoracion(this.getValoracion() + pan.getValorPan());
	}
}
